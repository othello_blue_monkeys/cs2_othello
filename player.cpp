#include "player.h"
#include <iostream>
#include <cmath>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) 
{
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;   
    
    myBoard = new Board;
    if (side == BLACK)
    {
    	mySide = BLACK;
    	otherSide = WHITE;
    }
    else
    {
    	mySide = WHITE;
    	otherSide = BLACK;
    }
    
    // Heuristic for all locations
    for (int i = 0; i < 8; i++)
    {
    	for (int j = 0; j < 8; j++)
    	{
    		heuristic[i][j] = 1;
    	}
    }
    
    // Heuristic for corners
    heuristic[0][0] = 9;
    heuristic[7][7] = 9;
    heuristic[0][7] = 9;
    heuristic[7][0] = 9;
    
    // Heuristic for edges
    for (int i = 2; i < 6; i++)
    {
    	heuristic[0][i] = 3;
    	heuristic[7][i] = 3;
    	heuristic[i][0] = 3;
    	heuristic[i][7] = 3;
    }
    // Heuristic for bad corners
    heuristic[1][1] = -2;
    heuristic[6][6] = -2;
    heuristic[1][6] = -2;
    heuristic[6][1] = -2;
    
    // Heuristic for bad edges
    heuristic[1][0] = -1;
    heuristic[0][1] = -1;
    heuristic[6][0] = -1;
    heuristic[7][1] = -1;
    heuristic[0][6] = -1;
    heuristic[1][7] = -1;
    heuristic[6][7] = -1;
    heuristic[7][6] = -1;    
    
    
}

/*
 * Destructor for the player.
 */
Player::~Player() 
{
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{    

	if (opponentsMove != NULL)
		myBoard->doMove(opponentsMove, otherSide);
	if (myBoard->isDone())
		return NULL;
	if (!myBoard->hasMoves(mySide))
		return NULL;
	
	// List of valid moves for 1 ply	
	std::vector<Move *> moves;
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			Move *myMove = new Move(i, j);
			if (myBoard->checkMove(myMove, mySide))
			{
				moves.push_back(myMove);
			}
		}
	}
	
	// Loops through all the legal moves
	int totalValue = -100000;
	Move *firstMove = NULL;
	for (int k = 0; k < (int) moves.size(); k++)
	{
		Board *myNewBoard = myBoard->copy();
		myNewBoard->doMove(moves[k], mySide);
		int minValue = 100000;
		Move *secondMove;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				// Finds the second legal move 
				Move *otherMove = new Move(i, j);
				if (myNewBoard->checkMove(otherMove, otherSide))
				{
					Board *secondDepth = myNewBoard->copy();
					secondDepth->doMove(otherMove, otherSide);
					
					// List of valid moves for third level
					std::vector<Move *> thirdMoves;
					for (int a = 0; a < 8; a++)
					{
						for (int b = 0; b < 8; b++)
						{
							Move *thirdMove = new Move(a, b);
							if (secondDepth->checkMove(thirdMove, mySide))
							{
								thirdMoves.push_back(thirdMove);
							}
							
						}
					}
					
					int fourTotalValue = -100000;
					Move *finalthirdMove;
					for (int c = 0; c < (int) thirdMoves.size(); c++)
					{
						Board *fourthBoard = secondDepth->copy();
						fourthBoard->doMove(thirdMoves[c], mySide);
						int fourminValue = 100000;
						Move *fourthMove;
						for (int d = 0; d < 8; d++)
						{
							for (int e = 0; e < 8; e++)
							{
								// Finds the second legal move 
								Move *otherfourthMove = new Move(d, e);
								if (fourthBoard->checkMove(otherfourthMove, otherSide))
								{
									Board *fifthBoard = fourthBoard->copy();
									fifthBoard->doMove(otherfourthMove, otherSide);
									
									std::vector<Move *> fifthMoves;
									for (int w = 0; w < 8; w++)
									{
										for (int x = 0; x < 8; x++)
										{
											Move *fifthMove = new Move(w, x);
											if (fifthBoard->checkMove(fifthMove, mySide))
											{
												fifthMoves.push_back(fifthMove);
											}
							
										}
									}
					
									int sixthTotalValue = -100000;
									Move *finalsixthMove;
									for (int y = 0; y < (int) fifthMoves.size(); y++)
									{
										Board *sixthBoard = fifthBoard->copy();
										sixthBoard->doMove(fifthMoves[y], mySide);
										int sixthminValue = 100000;
										Move *sixthMove;
										for (int aa = 0; aa < 8; aa++)
										{
											for (int bb = 0; bb < 8; bb++)
											{
												// Finds the second legal move 
												Move *othersixthMove = new Move(aa, bb);
												if (sixthBoard->checkMove(othersixthMove, otherSide))
												{
													Board *seventhBoard = sixthBoard->copy();
													seventhBoard->doMove(othersixthMove, otherSide);
													
													int mySideSum = 0;
													int otherSideSum = 0;
													for (int cc = 0; cc < 8; cc++)
													{
														for (int dd = 0; dd < 8; dd++)
														{
															if (seventhBoard->get(mySide, cc, dd))
																mySideSum += heuristic[cc][dd];
															else if (seventhBoard->get(otherSide, cc, dd))
																otherSideSum += heuristic[cc][dd];
														}
													}
													int newvalue = mySideSum - otherSideSum;
					
													// Checks for minimum move
													if (newvalue <= sixthminValue)
													{
														sixthminValue = newvalue;
														sixthMove = othersixthMove;				
													}
													
													delete seventhBoard;
												}
												delete othersixthMove;
											}
										}
					
										// Finds maximum of the minimum moves
										if (sixthminValue >= sixthTotalValue)
										{
											sixthTotalValue = sixthminValue;
											finalsixthMove = fifthMoves[y];
										}
										delete sixthBoard;
									}
					
									// Checks for minimum move
									if (sixthTotalValue <= fourminValue)
									{
										fourminValue = sixthTotalValue;
										fourthMove = finalsixthMove;				
									}
									delete fifthBoard;
								}
								delete otherfourthMove;
							}
						}
					
						// Finds maximum of the minimum moves
						if (fourminValue >= fourTotalValue)
						{
							fourTotalValue = fourminValue;
							finalthirdMove = thirdMoves[c];
						}
						delete fourthBoard;
					}
					
					if(fourTotalValue <= minValue)
					{
						minValue = fourTotalValue;
						secondMove = finalthirdMove;	
					}
					delete secondDepth;
				}
				delete otherMove;
			}
		}
		
			
		// Finds maximum of the minimum moves
		if (minValue >= totalValue)
		{
			totalValue = minValue;
			firstMove = moves[k];
		}
	}
	
	if (firstMove == NULL)
	{
		std::cerr << "Hello" << std::endl;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				Move *myMove = new Move(i, j);
				if (myBoard->checkMove(myMove, mySide))
				{
					myBoard->doMove(myMove, mySide);
					
					return myMove;
				}
			}
		}
		
	}
	
	myBoard->doMove(firstMove, mySide);
	return firstMove;
}

