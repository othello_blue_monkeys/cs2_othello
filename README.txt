Ramruthwick Pathireddy -- He implemented most of the heuristic code for the
project as well as worked on most of first week and some of the second week
part of the assignment.
  
Ramsathwick Pathireddy -- He did most of the work on the competitive AI and 
expanding the depth out program goes to. Also he helped out with both parts of
the assignment.





The improvements that we made to our AI is that we made the minimax algorithm
expand from a 2-ply to a 6-ply. We also changed the heuristic to be the sum of 
the values of each piece for each opponent. We made the corners have values
of 12, edges have 3, bad edges have -1, and bad corner have -2. This strategy
will work since we make the corners a higher priority and go deep enough to 
see an advantage. 
